public class Main {
    public static void main(String[] args) {
        EggAndChicken chicken = new EggAndChicken("Курица");
        EggAndChicken egg = new EggAndChicken("Яйцо");

        chicken.setOpponent(egg);
        egg.setOpponent(chicken);

        chicken.start();
        egg.start();

        try {
            chicken.join();
            egg.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}