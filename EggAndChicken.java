public class EggAndChicken extends Thread {
    private String name;
    private int count;
    private EggAndChicken oponent;

    public EggAndChicken(String name) {
        this.name = name;
    }

    public void setOpponent(EggAndChicken opponent) {
        oponent = opponent;
    }

    public void run() {
        while((count++) < 5_000) {
            System.out.println(name);
        }

        if(!oponent.isAlive()) {
            System.out.printf("Победителем становится: %s", name);
        }
    }
}